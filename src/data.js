export class Table {
    constructor(init) {
        this.init = init;
    }

    createHeader (data) {
        let open = "<thead><tr>";
        let close = "</tr></thead>";

        data.forEach((d) => {
            open += `<th>${d}</th>`;
        });

        return open + close;
    }

    createBody(data) {
        let open = "<tbody>";
        let close = "</tbody>";

        data.forEach((d) => {
            open += `
            <tr>
            <td>${d[0]}</td>
            <td>${d[1]}</td>
            <td>${d[2]}</td>
            <td>${d[3]}</td>
            <td>${d[4]}</td>
            <td>${d[5]}</td>
            </tr>
            `;
        });

        return open + close;
    }

    render(element) {
        let table =
        "<table class='table table-hover'>" +
        this.createHeader(this.init.columns) +
        this.createBody(this.init.data) +
        "</table>";
        element.innerHTML = table;
    }
}

function getData (url, cb){
    let xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status === 200) {
            return cb(JSON.parse(xhr.responseText));
        }
    };
    xhr.open("GET", url);
    xhr.send();
}

export function isiTable () {
    document.getElementById("app").innerHTML = `<div class='text-center'>Loading...</div>`;
    getData("https://jsonplaceholder.typicode.com/users/", (data) => {
        const dataTransformed = data.map((item) => [`${item.id}`, item.name, item.username,
        item.email, `${item.address.street}, ${item.address.suite}, ${item.address.city}`,
        item.company.name]);
        
        const table = new Table({
            columns: ["ID", "Name", "Username", "Email", "Address", "Company"],
            data: dataTransformed
        });
        
        const app = document.getElementById("app");
        table.render(app);
    });
}